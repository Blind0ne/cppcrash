#include <cstdio>
#include "Ball.h"

int myFunc(){
    printf("Print from my func\n");
    return 0;
}

int absolute_value(int x){
    if(x < 0) return -1*x;
    else return x;
}

int main(){
    int i = 29;
    bool abool = 42 == i;
    myFunc();
    printf("Hello World\n");
    printf("This is another test %i\n", i);
    Ball ball;
    ball.printsomething();

    if(i>30) printf("Bigger than 30\n");
    else printf("Less or equal than 30\n");

    int my_num = -10;
    printf("The absolute value of %d is %d.\n", my_num,absolute_value(my_num));

    return 0;
}
